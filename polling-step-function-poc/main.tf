provider "aws" {
  version = "~> 2.13"
}

provider "template" {
  version = "~> 2.1"
}

locals {
  unique_name = "wstampley"
  poc_name = "polling-poc"
  bucket_name = "${local.unique_name}-${local.poc_name}-bucket"
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "${local.poc_name}-lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_s3_bucket" "poc_bucket" {
  bucket = "${local.bucket_name}"
  acl = "private"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Allow",
      "Effect": "Allow",
      "Principal": "*",
      "Action": [
        "s3:*"
      ],
      "Resource": [
          "arn:aws:s3:::${local.bucket_name}/*",
          "arn:aws:s3:::${local.bucket_name}"
      ],
      "Condition": {
        "StringLike": {
          "aws:userid": [
            "${aws_iam_role.iam_for_lambda.unique_id}:*"
          ]
        }
      }
    }
  ]
}
EOF
}

resource "aws_lambda_function" "polling_lambda" {
  filename         = "polling-lambda/polling-lambda.zip"
  function_name    = "${local.poc_name}-polling-lambda"
  role             = "${aws_iam_role.iam_for_lambda.arn}"
  handler          = "index.handler"
  source_code_hash = "${base64sha256(file("polling-lambda/polling-lambda.zip"))}"
  runtime          = "nodejs8.10"
}

data "aws_iam_policy_document" "sfn_assume_role_policy_doc" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = ["states.us-east-1.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "step_function_role" {
  name = "${local.poc_name}-step-function-role"
  assume_role_policy = "${data.aws_iam_policy_document.sfn_assume_role_policy_doc.json}"
}

data "aws_iam_policy_document" "lambda-invoke" {
  statement {
    actions = [
      "lambda:InvokeFunction"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "lambda-invoke" {
    name = "${local.poc_name}-lambda-invoke-policy"
    policy = "${data.aws_iam_policy_document.lambda-invoke.json}"
}

resource "aws_iam_role_policy_attachment" "lambda-invoke" {
  role = "${aws_iam_role.step_function_role.name}"
  policy_arn = "${aws_iam_policy.lambda-invoke.arn}"
}

data "template_file" "step_function_definition" {
  template = "${file("polling-step-function/step-function-template.json")}"

  vars {
    polling-lambda-arn = "${aws_lambda_function.polling_lambda.arn}"
    bucket-name = "${aws_s3_bucket.poc_bucket.id}"
  }
}

resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = "${local.poc_name}-step-function"
  role_arn = "${aws_iam_role.step_function_role.arn}"

  definition = "${data.template_file.step_function_definition.rendered}"
}
