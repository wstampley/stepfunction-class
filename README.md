# Step Function Class

## Getting Started

### Install Amazon CLI

https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html

The Amazon CLI will enable us to validate security credentials and run test commands if necessary.

### Configure Amazon Profile

https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html

Follow these instructions to create a named profile for the account that you wish to use for this class.

### Install Terraform

https://learn.hashicorp.com/terraform/getting-started/install.html
https://www.terraform.io/downloads.html

Terraform provides us an easy way to setup infrastructure and deploy executables to AWS in a repeatable, maintainable way.

*NOTE: This project was tested against Terraform 0.11.4. Recently, Terraform 12.0 was released, and breaks a couple minor things here.*

## Stacks

### intro-step-function-poc

This stacks sets up components to demonstrate the basic step function step types.

It deploys the following major components:

  1. A Step Function
  1. A simple 'echo' Lambda

The step function runs through a series of each kind of state, demonstrating their basic format and behavior.

To deploy this stack, follow the steps below:

  1. Run `run-terraform.sh apply` and approve the results when it finishes calculating

To see this in action, follow the steps below:

  1. Launch an execution of the step function that was created
  1. Examine all the input and output steps in the call tree

To clean up this stack, follow the steps below:

  1. Run `run-terraform.sh destroy` and approve the results when it finishes calculating

### activity-step-function-poc

This stacks sets up components to demonstrate step function activities, implemented minimally in lambda for the POC.

It deploys the following major components:

  1. A Step Function Activity
  1. A Step Function
  1. An 'Activity Launcher' Lambda
  1. An 'Activity Metadata' S3 Bucket
  1. A 'Monitor' S3 Bucket
  1. A 'Monitor' Lambda

The Step Function queues executions into the activity. Running the Activity Launcher Lambda simulates real code calling to check for pending executions and writes activity metadata into the Activity Metadata bucket. Putting a `<id>/*.output.json` file into the Monitor bucket will trigger the monitor lambda, look up the Activity Metadata and finish off the step function execution.

To deploy this stack, follow the steps below:

  1. Change the `unique_name` local variable at the top of the stack to something unique.
  1. Run `run-terraform.sh apply` and approve the results when it finishes calculating

To see this in action, follow the steps below:

  1. Use the `sample-input\input.json` file to launch a copy of the step function that was created
  1. Verify that you have a step function execution that is in state `ActivityScheduled`
  1. Run the launcher lambda that was deployed
  1. Verify that this writes the task metadata to the metadata bucket that was created
  1. Verify that this moves the step function to `ActivityStarted`
  1. Upload the `sample-input\1.output.csv` file to `1/1.output.csv` in the monitor bucket
  1. Verify that this completes the step function execution successfully

To clean up this stack, follow the steps below:

  1. Empty the S3 buckets of all files (using the CLI or the amazon console)
  1. Run `run-terraform.sh destroy` and approve the results when it finishes calculating

### polling-step-function-poc

This stacks sets up components to demonstrate step functions that poll for a file in s3 (or anything else that can be checked from Lambda).

It deploys the following major components:

  1. A Step Function
  1. A S3 Bucket
  1. A 'Polling' Lambda

The Step Function sets state information to initialize the polling system. It then wait/poll cycles until it times out, or finds an `<id>/*.output.json` file in the bucket.

To deploy this stack, follow the steps below:

  1. Change the `unique_name` local variable at the top of the stack to something unique.
  1. Run `run-terraform.sh apply` and approve the results when it finishes calculating

To see this in action, follow the steps below:

  1. Use the `sample-input\input.json` file to launch a copy of the step function that was created
  1. Verify that you have a step function execution, and watch it go through the polling cycle at least once.
  1. Upload the `sample-input\1.output.csv` file to `1/1.output.csv` in the bucket
  1. Verify that this completes the step function execution successfully

Alternatively, to see it 'timeout' and give up polling, run one without uploading the output file and verify that it eventually ends.

To clean up this stack, follow the steps below:

  1. Empty the S3 bucket of all files (using the CLI or the Amazon console)
  1. Run `run-terraform.sh destroy` and approve the results when it finishes calculating
