provider "aws" {
  version = "~> 2.13"
}

provider "template" {
  version = "~> 2.1"
}

locals {
  unique_name = "wstampley"
  poc_name = "activity-poc"
  monitor_bucket_name = "${local.unique_name}-activity-poc-monitor-bucket"
  metadata_bucket_name = "${local.unique_name}-activity-poc-metadata-bucket"
}

data "aws_iam_policy_document" "iam_policy_for_lambda" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]
    principals {
      type = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "activity_policy_doc_for_lambda" {
  statement {
    actions = [
      "states:GetActivityTask",
      "states:SendTaskSuccess"
    ]
    resources = [
      "${aws_sfn_activity.step_function_activity.id}*"
    ]
  }
}

resource "aws_iam_policy" "activity_policy_for_lambda" {
    name = "${local.poc_name}-activity-invoke-policy"
    policy = "${data.aws_iam_policy_document.activity_policy_doc_for_lambda.json}"
}

resource "aws_iam_role" "iam_role_for_lambda" {
  name = "${local.poc_name}-lambda-role"

  assume_role_policy = "${data.aws_iam_policy_document.iam_policy_for_lambda.json}"
}

resource "aws_iam_role_policy_attachment" "activity_policy_attachment_for_lambda" {
  role = "${aws_iam_role.iam_role_for_lambda.name}"
  policy_arn = "${aws_iam_policy.activity_policy_for_lambda.arn}"
}

data "aws_iam_policy_document" "lambda_logging_document" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }
}

resource "aws_iam_policy" "lambda_logging_policy" {
  name = "${local.poc_name}-lambda-logging-policy"
  path = "/"
  description = "IAM policy for logging from a lambda"

  policy = "${data.aws_iam_policy_document.lambda_logging_document.json}"
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role = "${aws_iam_role.iam_role_for_lambda.name}"
  policy_arn = "${aws_iam_policy.lambda_logging_policy.arn}"
}

data "aws_iam_policy_document" "monitor_bucket_policy_document" {
  statement {
    actions = [
      "s3:*"
    ]
    principals = {
      type = "*"
      identifiers = ["*"]
    }
    resources = [
      "arn:aws:s3:::${local.monitor_bucket_name}/*",
      "arn:aws:s3:::${local.monitor_bucket_name}"
    ]
    condition {
      test = "StringLike"
      variable = "aws:userid"
      values = [
        "${aws_iam_role.iam_role_for_lambda.unique_id}:*"
      ]
    }
  }
}

data "aws_iam_policy_document" "metadata_bucket_policy_document" {
  statement {
    actions = [
      "s3:*"
    ]
    principals = {
      type = "*"
      identifiers = ["*"]
    }
    resources = [
      "arn:aws:s3:::${local.metadata_bucket_name}/*",
      "arn:aws:s3:::${local.metadata_bucket_name}"
    ]
    condition {
      test = "StringLike"
      variable = "aws:userid"
      values = [
        "${aws_iam_role.iam_role_for_lambda.unique_id}:*"
      ]
    }
  }
}

resource "aws_s3_bucket" "monitor_bucket" {
  bucket = "${local.monitor_bucket_name}"
  acl = "private"

  policy = "${data.aws_iam_policy_document.monitor_bucket_policy_document.json}"
}

resource "aws_s3_bucket" "metadata_bucket" {
  bucket = "${local.metadata_bucket_name}"
  acl = "private"

  policy = "${data.aws_iam_policy_document.metadata_bucket_policy_document.json}"
}

data "aws_iam_policy_document" "sfn_assume_role_policy_doc" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = ["states.us-east-1.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "step_function_role" {
  name = "${local.poc_name}-step-function-role"
  assume_role_policy = "${data.aws_iam_policy_document.sfn_assume_role_policy_doc.json}"
}

data "aws_iam_policy_document" "lambda_invoke_policy_document" {
  statement {
    actions = [
      "lambda:InvokeFunction"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "lambda_invoke_policy_for_step_function" {
    name = "${local.poc_name}-lambda-invoke-policy"
    policy = "${data.aws_iam_policy_document.lambda_invoke_policy_document.json}"
}

resource "aws_iam_role_policy_attachment" "lambda_policy_attachment_to_step_function" {
  role = "${aws_iam_role.step_function_role.name}"
  policy_arn = "${aws_iam_policy.lambda_invoke_policy_for_step_function.arn}"
}

resource "aws_sfn_activity" "step_function_activity" {
  name = "${local.poc_name}-activity"
}

data "template_file" "step_function_definition" {
  template = "${file("activity-step-function/step-function-template.json")}"

  vars {
    activity_arn = "${aws_sfn_activity.step_function_activity.id}"
    monitor_bucket_name = "${aws_s3_bucket.monitor_bucket.id}"
    metadata_bucket_name = "${aws_s3_bucket.metadata_bucket.id}"
  }
}

resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = "${local.poc_name}-step-function"
  role_arn = "${aws_iam_role.step_function_role.arn}"

  definition = "${data.template_file.step_function_definition.rendered}"
}

resource "aws_lambda_function" "launcher_lambda" {
  filename         = "launcher-lambda/launcher-lambda.zip"
  function_name    = "${local.poc_name}-launcher-lambda"
  role             = "${aws_iam_role.iam_role_for_lambda.arn}"
  handler          = "index.handler"
  source_code_hash = "${base64sha256(file("launcher-lambda/launcher-lambda.zip"))}"
  runtime          = "nodejs8.10"
  timeout          = "65"

  environment {
    variables = {
      ACTIVITY_ARN = "${aws_sfn_activity.step_function_activity.id}"
    }
  }
}

resource "aws_cloudwatch_event_rule" "every_five_minutes" {
  name = "every-five-minutes"
  description = "Fires every five minutes"
  schedule_expression = "rate(5 minutes)"
}

resource "aws_cloudwatch_event_target" "check_foo_every_five_minutes" {
  count = 0 // Disabling timer for POC

  rule = "${aws_cloudwatch_event_rule.every_five_minutes.name}"
  target_id = "launcher_lambda"
  arn = "${aws_lambda_function.launcher_lambda.arn}"
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_launcher" {
  statement_id = "AllowExecutionFromCloudWatch"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.launcher_lambda.function_name}"
  principal = "events.amazonaws.com"
  source_arn = "${aws_cloudwatch_event_rule.every_five_minutes.arn}"
}

resource "aws_lambda_function" "product_monitor_lambda" {
  filename         = "monitor-lambda/monitor-lambda.zip"
  function_name    = "${local.poc_name}-monitor-lambda"
  role             = "${aws_iam_role.iam_role_for_lambda.arn}"
  handler          = "index.handler"
  source_code_hash = "${base64sha256(file("monitor-lambda/monitor-lambda.zip"))}"
  runtime          = "nodejs8.10"
  timeout          = "15"

  environment {
    variables = {
      METADATA_BUCKET = "${aws_s3_bucket.metadata_bucket.id}"
    }
  }
}

resource "aws_lambda_permission" "allow_bucket_to_call_monitor" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.product_monitor_lambda.arn}"
  principal     = "s3.amazonaws.com"
  source_arn    = "${aws_s3_bucket.monitor_bucket.arn}"
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.monitor_bucket.id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.product_monitor_lambda.arn}"
    events              = ["s3:ObjectCreated:*"]
    filter_suffix       = ".output.csv"
  }
}
