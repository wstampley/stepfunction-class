#!/usr/bin/env bash

if [ -z "$AWS_PROFILE" ]
then
  echo "AWS Profile not set"
  exit 1
fi

TF_ACTION=${1:-"plan"}

# clean up local terraform state
rm -rf .terraform

# AWS Region
export AWS_DEFAULT_REGION=us-east-1

# Prepare Lambda Source Code
cd launcher-lambda/
rm launcher-lambda.zip
zip ./launcher-lambda.zip -r index.js
cd ..

cd monitor-lambda/
rm monitor-lambda.zip
zip ./monitor-lambda.zip -r index.js
cd ..

# Execute the Terraform
terraform init
terraform $TF_ACTION
